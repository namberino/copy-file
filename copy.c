#include <stdio.h>
#include <stdlib.h>

int main(int argc, char* argv[])
{
	if (argc < 3)
	{
		printf("Usage: ./copy <from_file> <to_file>\n");
		exit(1);
	}

    FILE *fp1, *fp2;
    char c;

    fp1 = fopen(argv[1], "r");
    if (fp1 == NULL)
    {
        printf("Cannot open file %s\n", argv[1]);
        exit(1);
    }

    fp2 = fopen(argv[2], "w");
    if (fp2 == NULL)
    {
        printf("Cannot open file %s\n", argv[2]);
        exit(1);
    }

	printf("Copying contents from %s to %s...\n", argv[1], argv[2]);
    // read contents from file
    c = fgetc(fp1);
    while (c != EOF)
    {
        fputc(c, fp2);
        c = fgetc(fp1);
    }

    printf("Contents copied to %s\n", argv[2]);

    fclose(fp1);
    fclose(fp2);

    return 0;
}
